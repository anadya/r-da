[1] "----------------------------------------------------------------------------------------------------"
[1] "Linear Regression Variable Selection StepWise Forward"
[1] "Candidate(s):  nrev.code, sustscore.code, score, organic"
[1] "----------------------------------------------------------------------------------------------------"
[1] "STEP  1 : Simple Linear Regression for predictors:  nrev.code, sustscore.code, score, organic"
[1] "Linear regression result"
       predictor            p   rSquared
3          score 7.000884e-06 0.10007484
4        organic 6.701370e-05 0.08126968
2 sustscore.code 1.338525e-02 0.03209965
1      nrev.code 7.385249e-02 0.01654683
[1] "Selected predictor : score  based on highest R Squared  0.100074835189387"
[1] "----------------------------------------------------------------------------------------------------"
[1] "STEP 2  Multiple Linear Regresion. Selected predictors : score"
[1] "Candidate(s):  nrev.code, sustscore.code, organic"
[1] "Linear regression result"
     X1             X2      pValues
3 score        organic 0.0001746832
2 score sustscore.code 0.0346290529
1 score      nrev.code 0.0846749064
[1] 3
[1] "Selected predictor(s) score,organic"
[1] "----------------------------------------------------------------------------------------------------"
[1] "STEP 3  Multiple Linear Regresion. Selected predictors : score,organic"
[1] "Candidate(s):  nrev.code, sustscore.code"
[1] "Linear regression result"
     X1      X2             X3   pValues
1 score organic      nrev.code 0.2546729
2 score organic sustscore.code 0.3059371
[1] "Selected predictor(s) score,organic"
[1] "----------------------------------------------------------------------------------------------------"
[1] "Decision: selected predictor(s) for best fit model:  score,organic"
[1] "Normal Distribution Observarion for variable: Residual"
    Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
-24.5838  -4.0862  -0.6316   0.0000   3.5548  51.5434 
[1] "skewness: 1.07975848410674"
[1] "kurtosis: 8.37230105456671"
[1] "Saphiro test for  Residual"
[1] "p-value is  2.12712149437382e-08"
[1] "Shapiro Test for  Residual  with alpha 0.05"

	Shapiro-Wilk normality test

data:  targetData
W = 0.92367, p-value = 2.127e-08

[1] "p-value is less than 0.05 H0 is rejected,  Residual  is not normally distributed"